import React from "react";
import { useState } from "react";

export const Button = (props: any) => {
    const [count, setCount] = useState(0);
  
    const onButtonClick = () => {
      setCount((oldCount) => ++oldCount);
    };
  
    return (
    <button onClick={onButtonClick} className="button">{props.children(count)}</button>
    );
  };
