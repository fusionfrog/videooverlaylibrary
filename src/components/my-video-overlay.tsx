import React from 'react';
import { VideoOverlayICL } from '../lib/video-overlay-icl/example/node_modules/video-overlay-icl/dist';
import script from './overlay-script.json';

export const MyVideoOverlay = () => {

const overlayConfiguration = { 
  text: "Example Text 😄",
  classNames: ['purple'],
  script: script,
  overlayReaders: ['overlayCommentary','overlayTitle'],
  clearStyle: 'clear',
  readerListener: 'videoEvent',
  action: () => {
      // generic independent action
      setTimeout(() => {
        const myOverlay = document.querySelector('.overlayLogo');
        myOverlay?.classList.remove('start');
        myOverlay?.classList.add('logo');
        myOverlay?.classList.add('fade-in');
        
        }, 5000);
    },
    overlays: [ 
      {
        text: "I love this animation 😄",
        classNames: ['overlayCommentary','start'],
      },
      {
        text: "I am hidden 😄",
        classNames: ['overlayTitle','start'],
      },
      {
        text: "VideoOverlay ICL",
        classNames: ['overlayLogo', 'start']
      }
  ]
}
  return (
    <div>
        <VideoOverlayICL config={ overlayConfiguration } />
    </div>
  );
}