import React from 'react';
import { render, screen } from '@testing-library/react';
import App from './App';

test('renders main react project', (): void => {
  render(<App />);
  const linkElement: HTMLElement = screen.getByText(/Video Player Main App/i);
  expect(linkElement).toBeInTheDocument();
});
