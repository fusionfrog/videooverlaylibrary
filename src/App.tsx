import React, { useEffect, useRef } from 'react';
import ShakaPlayer from 'shaka-player-react'
import './styles/App.css';
import './styles/videoOverlay.css';
import './styles/controls.css';
import './styles/demo.css';
import { MyVideoOverlay } from './components/my-video-overlay';

const STREAMS = [
  {
    name: 'Angel One MPEG-DASH',
    src: 'https://storage.googleapis.com/shaka-demo-assets/angel-one/dash.mpd'
  },
  {
    name: 'Big Buck Bunny HLS',
    src:
      'https://storage.googleapis.com/shaka-demo-assets/bbb-dark-truths-hls/hls.m3u8'
  },
  {
    name: 'Kurzgesagt',
    src:
      'testvideo.mp4'
  }
];

const dispatchVideoEvents= (ref, lastTime) => {
  setTimeout(() => {
    let currentTime: number = 0;
    if (ref) currentTime = ref.videoElement.currentTime;
    if (Math.floor(currentTime) !== Math.floor(lastTime)) {
      const event = new CustomEvent("videoEvent", {detail: ref}) as CustomEvent;
      window.dispatchEvent(event);
    }
    lastTime = currentTime;
    setTimeout(dispatchVideoEvents(ref, lastTime), 0);
  }, 100);
}

const App: React.FC = () => {
  const controllerRef = useRef(null);

  useEffect(() => {
   // @ts-ignore: Unreachable code error
    window.getShakaInst = controllerRef.current;
    const lastTime: number = 0;
    dispatchVideoEvents(controllerRef.current, lastTime);
  }, []);

  const [src, setSrc] = React.useState(STREAMS[2].src);

  function onSelectSrc(event) {
    setSrc(event.target.value);
  }

  return (
    <div>
      <div>
        <select value={src} onChange={onSelectSrc}>
          {STREAMS.map((stream, index) => (
            <option key={index} value={stream.src}>{stream.name}</option>
          ))}
        </select>
      </div>
      <div className="playerWindow flexstart">
        <MyVideoOverlay />
        <ShakaPlayer autoPlay ref={controllerRef} src={src} />
      </div>
    </div>
  );
}

export default App;
 