import React, { useState } from "react";

export const Happy = (props: any) => {
    const [count, setCount] = useState(0);
  
    const onButtonClick = () => {
      setCount((oldCount) => ++oldCount);
    };
  
    return (
    <button onClick={onButtonClick} className="button">{props.children(count)}</button>
    );
  };
