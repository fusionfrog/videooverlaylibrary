import * as React from 'react'
import { useEffect } from 'react';

interface Props {
    config: {
      overlays?: {
        text: string, 
        classNames: string[]
      } [],
      text: string,
      classNames?: string[],
      script?: any,
      overlayReaders?: string[],
      clearStyle?: string,
      readerListener?: string,
      action?: any
    }
}

export const VideoOverlayICL: React.FC<Props> = ({config}) => {

  useEffect(() => {
    if (config.action) config.action ();
  });

  if (config.script) {
    if (!config.overlayReaders) {
      console.warn('At least one overlay reader id is required when script is supplied.');
    } else {
      const readerActions = (e: any) => { 
        const clearStyle = config.clearStyle ? config.clearStyle : 'clear';
        const videoInstance = e.detail;
        const timeCode = videoInstance.videoElement.currentTime;
        const filename = videoInstance.videoElement.src.substr(videoInstance.videoElement.src.lastIndexOf('/') + 1);
        if (config.overlayReaders) {
          config.overlayReaders.forEach( (reader) => {
            scriptReader(
              {
                overlayId: reader,
                clearStyle,
                filename,
                timeCode,
              }
            );
          })
        }
      }
      // add event listener
      const readerListener = config.readerListener ? config.readerListener : 'videoEvent';
      window.addEventListener(readerListener, readerActions);

    }
    const scriptReader = ({...reader}) => {
      const myOverlay: any = document.querySelector('.' + reader.overlayId);  
      if (config.script[reader.filename]) {
        if (config.script[reader.filename][reader.overlayId]) {
          let match = false;
          config.script[reader.filename][reader.overlayId].actions.forEach( (action: any) => {
            if (action.timeStart <= reader.timeCode && action.timeEnd >= reader.timeCode) {
              myOverlay.className = `${reader.overlayId} ${action.style}`;
              myOverlay.innerHTML = action.text;
              match = true;
            }
          })
          if (!match) {
            myOverlay.className = `${reader.overlayId} ${reader.clearStyle}`;
            }
        }
      }
    }
  }

  
  const Overlays = () => (
    <div className="overlays">
    {config.overlays ? config.overlays.map((overlay, index) => (
      <div key={index} className={overlay.classNames.join(' ')} >
          {overlay.text}
      </div>
      
    )) : <div>Overlay array not passed.</div>}
    </div>
  ) 
 
  return (
  <div>
    <Overlays />
  </div>
  ) 
}
