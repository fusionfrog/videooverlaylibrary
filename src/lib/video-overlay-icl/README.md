# video-overlay-icl

> Video Overlay library

[![NPM](https://img.shields.io/npm/v/video-overlay-icl.svg)](https://www.npmjs.com/package/video-overlay-icl) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Install

```bash
npm install --save video-overlay-icl
```

## Usage

```tsx
import React, { Component } from 'react'

import MyComponent from 'video-overlay-icl'
import 'video-overlay-icl/dist/index.css'

class Example extends Component {
  render() {
    return <MyComponent />
  }
}
```

