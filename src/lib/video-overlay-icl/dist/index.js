var React = require('react');

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

var VideoOverlayICL = function VideoOverlayICL(_ref) {
  var config = _ref.config;
  React.useEffect(function () {
    if (config.action) config.action();
  });

  if (config.script) {
    if (!config.overlayReaders) {
      console.warn('At least one overlay reader id is required when script is supplied.');
    } else {
      var readerActions = function readerActions(e) {
        var clearStyle = config.clearStyle ? config.clearStyle : 'clear';
        var videoInstance = e.detail;
        var timeCode = videoInstance.videoElement.currentTime;
        var filename = videoInstance.videoElement.src.substr(videoInstance.videoElement.src.lastIndexOf('/') + 1);

        if (config.overlayReaders) {
          config.overlayReaders.forEach(function (reader) {
            scriptReader({
              overlayId: reader,
              clearStyle: clearStyle,
              filename: filename,
              timeCode: timeCode
            });
          });
        }
      };

      var readerListener = config.readerListener ? config.readerListener : 'videoEvent';
      window.addEventListener(readerListener, readerActions);
    }

    var scriptReader = function scriptReader(_ref2) {
      var reader = _extends({}, _ref2);

      var myOverlay = document.querySelector('.' + reader.overlayId);

      if (config.script[reader.filename]) {
        if (config.script[reader.filename][reader.overlayId]) {
          var match = false;
          config.script[reader.filename][reader.overlayId].actions.forEach(function (action) {
            if (action.timeStart <= reader.timeCode && action.timeEnd >= reader.timeCode) {
              myOverlay.className = reader.overlayId + " " + action.style;
              myOverlay.innerHTML = action.text;
              match = true;
            }
          });

          if (!match) {
            myOverlay.className = reader.overlayId + " " + reader.clearStyle;
          }
        }
      }
    };
  }

  var Overlays = function Overlays() {
    return React.createElement("div", {
      className: "overlays"
    }, config.overlays ? config.overlays.map(function (overlay, index) {
      return React.createElement("div", {
        key: index,
        className: overlay.classNames.join(' ')
      }, overlay.text);
    }) : React.createElement("div", null, "Overlay array not passed."));
  };

  return React.createElement("div", null, React.createElement(Overlays, null));
};

exports.VideoOverlayICL = VideoOverlayICL;
//# sourceMappingURL=index.js.map
