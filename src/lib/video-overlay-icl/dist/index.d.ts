import * as React from 'react';
interface Props {
    config: {
        overlays?: {
            text: string;
            classNames: string[];
        }[];
        text: string;
        classNames?: string[];
        script?: any;
        overlayReaders?: string[];
        clearStyle?: string;
        readerListener?: string;
        action?: any;
    };
}
export declare const VideoOverlayICL: React.FC<Props>;
export {};
