import { useEffect, createElement } from 'react';

const VideoOverlayICL = ({
  config
}) => {
  useEffect(() => {
    if (config.action) config.action();
  });

  if (config.script) {
    if (!config.overlayReaders) {
      console.warn('At least one overlay reader id is required when script is supplied.');
    } else {
      const readerActions = e => {
        const clearStyle = config.clearStyle ? config.clearStyle : 'clear';
        const videoInstance = e.detail;
        const timeCode = videoInstance.videoElement.currentTime;
        const filename = videoInstance.videoElement.src.substr(videoInstance.videoElement.src.lastIndexOf('/') + 1);

        if (config.overlayReaders) {
          config.overlayReaders.forEach(reader => {
            scriptReader({
              overlayId: reader,
              clearStyle,
              filename,
              timeCode
            });
          });
        }
      };

      const readerListener = config.readerListener ? config.readerListener : 'videoEvent';
      window.addEventListener(readerListener, readerActions);
    }

    const scriptReader = ({ ...reader
    }) => {
      const myOverlay = document.querySelector('.' + reader.overlayId);

      if (config.script[reader.filename]) {
        if (config.script[reader.filename][reader.overlayId]) {
          let match = false;
          config.script[reader.filename][reader.overlayId].actions.forEach(action => {
            if (action.timeStart <= reader.timeCode && action.timeEnd >= reader.timeCode) {
              myOverlay.className = `${reader.overlayId} ${action.style}`;
              myOverlay.innerHTML = action.text;
              match = true;
            }
          });

          if (!match) {
            myOverlay.className = `${reader.overlayId} ${reader.clearStyle}`;
          }
        }
      }
    };
  }

  const Overlays = () => createElement("div", {
    className: "overlays"
  }, config.overlays ? config.overlays.map((overlay, index) => createElement("div", {
    key: index,
    className: overlay.classNames.join(' ')
  }, overlay.text)) : createElement("div", null, "Overlay array not passed."));

  return createElement("div", null, createElement(Overlays, null));
};

export { VideoOverlayICL };
//# sourceMappingURL=index.modern.js.map
