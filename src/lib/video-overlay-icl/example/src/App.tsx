import React from 'react'

import { VideoOverlayICL } from 'video-overlay-icl'
import 'video-overlay-icl/dist/index.css'

const App = () => {
  const testConfig = { 
    text: "Example Text 😄",
    classNames: ['voTestComponentStyle'],
    overlays: [ 
      {
        text: "Overlay 1 😄",
        classNames: ['voTestComponentStyle']
      },
      {
        text: "Overlay 2 😄",
        classNames: ['voTestComponentStyle']
      }
    ]
  }
  return <VideoOverlayICL config={testConfig} />
}

export default App
