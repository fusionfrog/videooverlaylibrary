# Video Overlay Library

An inversion control library

## Overview

This is a React/TypeScript project that demonstrates a simple but highly extensive inversion control library

The project is composed of a main app utilizing Shaka Player
and a video-overlay-icl library
For development, the library resides in the 'lib' directory. When moving to production this would become a npm module.

## Design

The library was build as React component to acommodate unlimited overlays each with complex customization.
Configuration object can be passed to the library to affect behavior, text and style.
Additionaly the library supports json script language to define behavior based on video timecode.

Since it is built around Shaka Player the presently dependent on reading the timecode from the player object. This could be abstracted in the future.

## Install

```bash
yarn
yarn start
```

You can directly affect the behavior of the overlays by editing the following two files.
```tsx

src/components/overlay-script.json
src/styles/videoOverlay.css

```

## Creating an overlay

The configuration object is used to configure your overlays

### Basic overlay
```tsx

import { VideoOverlayICL } from '../lib/video-overlay-icl/example/node_modules/video-overlay-icl/dist';

export const MyVideoOverlay = () => {

const overlayConfiguration = {
    overlays: [ 
      {
        text: "Your logo 😄",
        classNames: ['myLogo','start'],
      }
  ]
}
  return (
    <div>
        <VideoOverlayICL config={ overlayConfiguration } />
    </div>
  );
  ```
### Overlay with actions

```tsx
const overlayConfiguration = { 
  action: () => {
      // generic independent action
      setTimeout(() => {
        const myOverlay = document.querySelector('.overlayLogo');
        myOverlay?.classList.remove('start');
        myOverlay?.classList.add('logo');
        myOverlay?.classList.add('fade-in');
        
        }, 5000);
    },
    overlays: [ 
      {
        text: "VideoOverlay ICL",
        classNames: ['overlayLogo', 'start']
      }
  ]
}
```

### Overlays controlled by a script

Script controll requires two dependencies.
* Script file
* Video event dispatcher

Script is a json file uses key-value pairs for fast lookup and uses the following keys.
Video filename, overlayName and actions


```tsx
{
    "testvideo.mp4": {   
        "name": "Kurzgesagt",
        "overlayCommentary": {
            "actions": [
                {
                    "timeStart": "10",
                    "timeEnd": "19",
                    "text": "Welcome to your future.",
                    "style": "impressionOne"
                }                
        }
    }
}
```

Example of a video event dispatcher

```tsx
const dispatchVideoEvents= (ref, lastTime) => {
  setTimeout(() => {
    let currentTime: number = 0;
    if (ref) currentTime = ref.videoElement.currentTime;
    if (Math.floor(currentTime) !== Math.floor(lastTime)) {
      const event = new CustomEvent("videoEvent", {detail: ref}) as CustomEvent;
      window.dispatchEvent(event);
    }
    lastTime = currentTime;
    setTimeout(dispatchVideoEvents(ref, lastTime), 0);
  }, 100);
}
```

Configuration object where we pass the script, name of the eventlistener, 

```tsx
import script from './overlay-script.json';

export const MyVideoOverlay = () => {

const overlayConfiguration = { 
    script: script,
    overlayReaders: ['overlayCommentary','overlayTitle'],
    clearStyle: 'clear',
    readerListener: 'videoEvent',
    overlays: [ 
        {
        text: "My fav 😄",
        classNames: ['overlayCommentary','start'],
        }
}
```

### Demostration
You can see a live demostration here
[VideoOverlay ICL demo](https://nbc-video-overlay.netlify.app/)

Cheers
